import React from 'react'

import './styles.css'

import logo_home from '../../assets/logo-home.png'
import ic_cadeado from '../../assets/ic-cadeado.png'
import ic_email from '../../assets/ic-email.png'




export default function Login() {
    return (
        <div className="container text-center align-center ">
            <header>
                <img src={logo_home} alt="logo home" id="logo_home" />
            </header>
            <section className="flex-column">
                <h1 id="bem-vindo">BEM-VINDO AO <br/> EMPRESAS</h1>
                <p id="lorem-ipsum">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
                <form>
                    <div className="form-group">
                        <img src={ic_email} id="ic_email"></img>
                        <input type="text" className="input_form" id="" placeholder="E-mail" />
                    </div>
                    <div className="form-group">
                        <img src={ic_cadeado} id="ic_cadeado"></img>
                        <input type="password" className="input_form" id="" placeholder="Senha" />
                    </div>
                    <button className="" id="submit_btn"><span id="enter">ENTRAR</span></button>
                </form>
            </section>
        </div>
    )
}

// <div className="App container text-center align-center mt-5" id="box">