import React from 'react';

import './global.css'

import Routes from './routes'


function App() {
  return (
    <div className="container mt-5" id="box">
      <Routes />
    </div>
  );
}

export default App;
